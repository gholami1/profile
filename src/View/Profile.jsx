import React, { useState, useEffect } from "react";

const Profile = () => {

    const [name, setName] = useState("");
    const [cat, setCat] = useState("");
    const [url, setUrl] = useState("");

    useEffect(() => {}, []);

    const getFile = (evt) => {
        var tgt = evt.target || window.event.srcElement,
        files = tgt.files;

        // FileReader support
        if (FileReader && files && files.length) {
        var fr = new FileReader();
        fr.onload = function () {
            const arr = [...url];
            setUrl([...arr, fr.result]);
        };
        fr.readAsDataURL(files[0]);
        }
    };

    const handleDelete = (id) => {
            const del = url.filter((item,index) => index !== id );
            setUrl(del);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(url , name , cat , 'url')
    }

    return (
        <div className="col-12 col-lg-3 border mx-auto bg-light">
            <form onSubmit={handleSubmit}>
                <section className="dir-rtl profile">
                    <div className="d-flex mt-4">
                        <div className="col-5 text-right">
                            <i className="icon icon-35 icon-profile"></i>
                        </div>
                        <div className="col-7 background-color-gray border-radius-30 form-group">
                        <select className="form-control background-color-gray border-none text-12" name="cat" value={cat} onChange={e => setCat(e.target.value)}>
                            <option className="">انتخاب دسته بندی</option>
                            <option>1</option>
                            <option>2</option>
                        </select>
                        </div>
                    </div>
                    <h4 className="title text-right text-18 mt-2 font-vazir">عنوان مطلب</h4>
                    <div className="form-group col-8 pr-0 pb-5 mt-2">
                        <input
                            onChange={e => setName(e.target.value)}
                            name="name"
                            value={name}
                            autoComplete="off"
                            type="text"
                            className="form-control border-none font-vazir text-12"
                            placeholder="متن مورد نظر خود را تایپ کنید"
                        />
                    </div>
                    <div className="d-flex flex-wrap">
                        {url
                        ? url.map((item, index) => {
                            const key = `${index}`;
                                return (
                                    <>
                                        <div
                                            key={key}
                                            className="position-relative"
                                            id={key}
                                            style={{
                                                flexGrow: 1,
                                                flexShrink: 1,
                                                flexBasis: "calc(50% - 30px)",
                                                height: 120,
                                                margin: 15,
                                                background: `url(${item})`,
                                                backgroundSize: "cover",
                                                borderRadius: 8
                                            }}
                                        >
                                            <i className="icon icon-20 icon-close position-absolute close-box" id={key} onClick={() => handleDelete(index)}></i>
                                        </div>
                                </>
                                );
                            })
                        : ""}
                    </div>
                </section>
                <footer className="d-flex justify-content-center">
                    <div className="col-lg-12 col-12 d-flex bg-light footer-row">
                        <div className="col-5">
                            <button className="btn btn-primary w-100">انتشار</button>
                        </div>
                        <div className="col-7 d-flex justify-content-end text-right pr-0 mt-2">
                          
                                <label for="exampleInputPassword1">
                                    <i className="icon icon-23 icon-picture"></i>
                                </label>
                                <input
                                    type="file"
                                    className="form-control position-absolute d-none"
                                    id="exampleInputPassword1"
                                    onChange={getFile}
                                />
                      
                            <i className="icon icon-22 icon-film ml-1"></i>
                            <i className="icon icon-22 icon-chart ml-1"></i>
                        </div>
                    </div>
                </footer>
            </form>
        </div>
    );
};

export default Profile;
